tests
========

usage

just spin up machines
```
vagrant up
```

specify server and token
```
export gl_reg_token=abc123
export gl_reg_url=https://gitlab.com
vagrant up
```
without a token, it will not attempt to register (this is default).

The URL may be omitted, and will the default to `https://gitlab.com`
